import ExternReport.Report;
import Helper.TestFunction;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;
import java.util.ArrayList;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;


public class SimpleTest {
    Report rep = new Report();
    private TestFunction test = new TestFunction();
    @BeforeTest
    public void setUpExtern() {
       rep.start();
    }

    @BeforeMethod
    void openPage() {
        test.openPage("https://yandex.ru/");
    }

    @Test
    void TestCase_0() throws InterruptedException {
        rep.nameTest("Test Case 1", "qwer");
        test.checkClick("Компьютерная техника");
        test.checkClick("Накопители данных");
        test.checkClick("Внешние жесткие диски и SSD");
        test.checkHeader("Внешние жесткие диски и SSD");
        test.priceTo("30000");
        test.manufacturer("Toshiba");
        test.manufacturer("ADATA");
        test.metod4812();
        Thread.sleep(3000); //костыль, ждем пока элементы подгрузятся
        int x = $$(byClassName("n-snippet-card2__title")).size();
        Assert.assertEquals(x, 12);
        String first = $$(byClassName("n-snippet-card2__title")).get(0).getText();
            $(byName("text")).setValue(first).pressEnter();
        $$(byClassName("n-snippet-card2__title")).get(0).click();
        String test = $(byXpath("//h1[@class='title title_size_28 title_bold_yes']")).getText();
        Assert.assertEquals(first, test);

    }

    @Test
    void TestCase_1() throws InterruptedException {
        rep.nameTest("Test Case 2", "qwer");
        test.checkClick("Компьютерная техника");
        test.checkClick("Оргтехника");
        test.checkClick("Принтеры и МФУ");
        test.checkHeader("Принтеры и МФУ");
        test.priceFrom("20000");
        test.priceTo("25000");
        test.manufacturer("HP");
        Thread.sleep(3000); //костыль, ждем пока элементы подгрузятся
        int x = $$(byClassName("n-snippet-card2__title")).size();
        while (x <= 1) {
            x = $$(byClassName("n-snippet-card2__title")).size();
        }
        ArrayList<String> list = new ArrayList<String>();
        ArrayList<Integer> listInt = new ArrayList<Integer>();
        for (int i = 0; i < x; i++) {
            list.add($$(byClassName("n-snippet-card2__main-price")).get(i).getText());
            listInt.add(Integer.valueOf(list.get(i).replaceAll("\\s+", "").replaceAll("₽", "")));
            System.out.println(listInt.get(i));
            if (listInt.get(i) > 25000 || listInt.get(i) < 20000) {
                Assert.fail("Error");
            }
        }
    }

    @AfterMethod
    public void getResult(ITestResult result) {
        rep.getResult(result);
    }

    @AfterTest
    void exit() {
        test.closePage();
        rep.close();
    }
}


