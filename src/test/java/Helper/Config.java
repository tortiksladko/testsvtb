package Helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class Config {
    public static String getDataProperties(String param){
        Properties props = new Properties();
        try {
            props.load(new InputStreamReader(new FileInputStream("application.properties"), "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props.getProperty(param);
    }
}
