package Helper;


import com.codeborne.selenide.Configuration;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.open;

public class TestFunction {
    Config cg = new Config();

    public void openPage(String url) {
        Configuration.browser = cg.getDataProperties("browser");
        Configuration.startMaximized = true;
        open(url);
        $(byText("Маркет")).shouldBe(exist);
        $(byText("Маркет")).click();
    }

    public void checkClick(String element) {
        $(byText(element)).shouldBe(exist);
        $(byText(element)).click();
    }

    public void metod4812() {
        $(byXpath("//button[@class='button button_theme_normal button_arrow_down button_size_s select__button i-bem button_js_inited']")).click();
        $(byXpath("//span[@class='select__text'][contains(text(),'12')]")).click();
    }

    public void manufacturer(String element) {
        $(byXpath("//div[@class='search-layout']")).$(byText(element)).click();
    }

    public void priceFrom(String price) {
        $(byId("glpricefrom")).setValue(price);
    }

    public void priceTo(String price) {
        $(byId("glpriceto")).setValue(price);
    }

    public void checkHeader(String element) {
        $(byClassName("headline__header")).$(byText(element)).isDisplayed();
    }

    public void closePage() {
        close();
    }
}
